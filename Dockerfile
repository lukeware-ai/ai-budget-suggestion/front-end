# Use uma imagem de Node.js como base
FROM node:latest

# Defina o diretório de trabalho no diretório do aplicativo
WORKDIR /usr/src/app

# Copie os arquivos necessários para o contêiner
COPY package.json yarn.lock ./
COPY .next ./.next

# Instale as dependências do projeto
RUN yarn install --production=true

# Exponha a porta configurada
EXPOSE 8883

# Defina o comando padrão para iniciar o aplicativo
CMD ["yarn", "start", "-p 8883"]
