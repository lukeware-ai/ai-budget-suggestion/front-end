import React, { createContext, useContext, useState, ReactNode, useEffect } from 'react';

type BudgetParameters = {
  hourlyRate: number;
  workTime: number;
  materialRate: number;
  complexity: number;
  costLunch: number;
  distanceWork: number;
  costFuel: number;
  vehicleMaintenanceRate: number;
  tax: number;
  workToolsRate: number;
  employeeWorkTime: number;
  costEmployeeHourly: number;
  numberEmployees: number;
  costMaterial: number;
  label?: number;
};

type BudgetContextType = {
  budgetParameters: BudgetParameters;
  updateBudgetParameter: (parameter: keyof BudgetParameters, value: number) => void;
  clearBudgetParameters: () => void;
};

const initialBudgetParameters: BudgetParameters = {
  hourlyRate: 0.0,
  workTime: 0.0,
  materialRate: 0.0,
  complexity: 0.0,
  costLunch: 0.0,
  distanceWork: 0.0,
  costFuel: 0.0,
  vehicleMaintenanceRate: 0.0,
  tax: 0.0,
  workToolsRate: 0.0,
  employeeWorkTime: 0.0,
  costEmployeeHourly: 0.0,
  numberEmployees: 0.0,
  costMaterial: 0.0,
};

const budget_parameters_context = createContext<BudgetContextType | undefined>(undefined);

export const useBudgetParameters = () => {
  const context = useContext(budget_parameters_context);
  if (!context) {
    throw new Error('useQuestionContext must be used within a QuestionProvider');
  }
  return context;
};

type BudgetParametersProviderProps = {
  children: ReactNode;
};

export const BudgetParametersProvider: React.FC<BudgetParametersProviderProps> = ({ children }) => {
  const [budgetParameters, setBudgetParameters] = useState<BudgetParameters>(initialBudgetParameters);

  useEffect(() => {
    const storedParameters = localStorage.getItem('budgetParameters');
    if (storedParameters) {
      setBudgetParameters(JSON.parse(storedParameters));
    }
  }, []);

  const setBudgetParameter = (parameter: keyof BudgetParameters, value: number) => {
    setBudgetParameters((prevParameters) => {
      const newParameters = { ...prevParameters, [parameter]: value };
      localStorage.setItem('budgetParameters', JSON.stringify(newParameters)); // Save to localStorage
      return newParameters;
    });
  };

  const clearBudgetParameters = () => {
    setBudgetParameters(initialBudgetParameters);
    localStorage.removeItem('budgetParameters');
  };

  return (
    <budget_parameters_context.Provider value={{ budgetParameters, updateBudgetParameter: setBudgetParameter, clearBudgetParameters }}>
      {children}
    </budget_parameters_context.Provider>
  );
};
