"use client";

import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { FcOk, FcHighPriority } from "react-icons/fc";
import { GoHomeFill } from "react-icons/go";
import { TypingEffect } from './components/typing_effect';

const TEXT = "E aí! Tudo certo? Aqui quem fala é o SO.AI. Estou aqui para te ajudar com as estimativas de orçamento para seus serviços. Queremos deixar tudo rápido e fácil para você. Só preciso que você responda algumas perguntinhas, aí eu te dou uma ideia de preço. Bora lá!"

export default function Page() {
  const router = useRouter();
  const [status, setStatus] = useState<boolean>(false);
  const [showTypingEffect, setShowTypingEffect] = useState<boolean>(false);


  useEffect(() => {
    const checkHealth = async () => {
      try {
        const response = await fetch("/api/check_health");
        const data = await response.json();
        setStatus(data.status === "success");
      } catch (error) {
        setStatus(false);
      }
    };

    checkHealth();

    const typingEffectShown = localStorage.getItem("typingEffectShown");
    if (!typingEffectShown) {
      setShowTypingEffect(true);
      localStorage.setItem("typingEffectShown", "true");
    }
  }, []);

  return (
    <>
      <div className="relative">
        <div className="absolute top-0 left-0 m-4">
          <span role="img" aria-label="Ícone para tela inicial" onClick={() => router.push("/")}>
            <GoHomeFill className="w-7 h-7 text-gray-700 cursor-pointer" />
          </span>
        </div>
      </div>
      <div className="relative">
        <div className="absolute top-0 right-0 m-4">
          {status === true && (
            <span role="img" aria-label="Ícone serviço esta conectado">
              <FcOk className="w-5 h-5 text-gray-700" />
            </span>
          )}
          {status === false && (
            <span role="img" aria-label="Ícone serviço fora do ar">
              <FcHighPriority className="w-5 h-5 text-gray-700" />
            </span>
          )}
        </div>
      </div>
      <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
        <div className="p-6 rounded-lg shadow-lg w-full max-w-4xl bg-white">
          <h1 className="text-base sm:text-2xl font-semibold mb-4 text-gray-700 text-justify">
            Refinando os orçamentos para microempreendedores com o auxílio da SO.AI para sugestões orçamentárias.
          </h1>
          <h2 className="text-[17px] sm:text-xl font-light text-justify mb-4 text-gray-700">
            {showTypingEffect ? (
              <TypingEffect text={TEXT} />
            ) : (
              <>{TEXT}</>
            )}
          </h2>
          <button aria-label='Botão iniciar a sugestão orçamentária'
            className="text-white py-4 px-12 rounded-2xl bg-purple-700 hover:bg-purple-600 ml-auto float-end uppercase my-4"
            onClick={() => router.push("/hourly_rate")}
          >
            Iniciar
          </button>
        </div>
      </div>
    </>
  );
}
