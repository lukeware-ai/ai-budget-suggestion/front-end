import { useEffect, useState } from 'react';
import { GoHomeFill } from 'react-icons/go';
import { TypingEffect } from './typing_effect';
import "@/app/globals.css";

interface TemplateProps {
    title: string;
    description: string;
    children: React.ReactNode;
    textEffect?: boolean;
    fadeIn?: boolean;
    onGoHome: () => void;
}

const Template: React.FC<TemplateProps> = ({
    title,
    description,
    children,
    textEffect = true,
    fadeIn = true,
    onGoHome
}) => {
    const [isVisible, setIsVisible] = useState(false);

    useEffect(() => {
        if (fadeIn) {
            const timer = setTimeout(() => setIsVisible(true), 10);
            return () => clearTimeout(timer);
        }
    }, [fadeIn]);

    return (
        <>
            <header>
                <div className="relative">
                    <div className="absolute top-0 left-0 m-4">
                        <span role="img" aria-label="Ícone para tela inicial" onClick={onGoHome}>
                            <GoHomeFill className="w-7 h-7 text-gray-700 cursor-pointer" />
                        </span>
                    </div>
                </div>
            </header>
            <main className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
                <section className="bg-white p-6 rounded-lg shadow-lg w-full max-w-4xl">
                    <h1 className="text-xl sm:text-2xl font-bold mb-4 text-gray-700 uppercase">
                        {title}
                    </h1>
                    <h2 className="text-lg sm:text-xl font-light mb-10 text-gray-700 text-justify">
                        {textEffect ? (
                            <TypingEffect text={description} />
                        ) : (
                            <>{description}</>
                        )}
                    </h2>
                    <div className={`fade-in ${isVisible ? 'fade-in-visible' : ''}`}>
                        {children}
                    </div>
                </section>
            </main>
        </>
    );
};

export { Template };
