import React from 'react';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

// Defina os tipos das opções da máscara
interface MaskOptions {
    placeholder: string;
    prefix?: string;
    suffix?: string;
    includeThousandsSeparator?: boolean;
    thousandsSeparatorSymbol?: string;
    allowDecimal?: boolean;
    decimalSymbol?: string;
    decimalLimit?: number;
    requireDecimal?: boolean;
    allowNegative?: boolean;
    allowLeadingZeroes?: boolean;
    integerLimit?: number;
}

// Defina os tipos das propriedades do componente
interface CurrencyInputProps  {
    maskOptions?: MaskOptions;
}

const defaultMaskOptions: MaskOptions = {
    placeholder: "",
    prefix: 'R$ ',
    suffix: '',
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: '',
    allowDecimal: true,
    decimalSymbol: ',',
    decimalLimit: 2, // Quantos dígitos permitidos após o decimal
    integerLimit: 8, // Limite do comprimento dos números inteiros
    allowNegative: false,
    allowLeadingZeroes: false,
};

const CurrencyInput: React.FC<CurrencyInputProps> = ({ maskOptions, ...inputProps }) => {
    const currencyMask = createNumberMask({
        ...maskOptions,
        ...defaultMaskOptions,
    });

    return <MaskedInput mask={currencyMask} {...inputProps} />;
};


export default CurrencyInput;
