import { memo } from 'react'

interface ButtonNavigationProps {
    goBackText?: string;
    goNextText?: string;
    onGoBack?: () => void;
    onNext: () => void;
}

const ButtonNavigation: React.FC<ButtonNavigationProps> = memo(({
    goBackText = "Voltar",
    goNextText = "Próximo",
    onGoBack,
    onNext
}) => {
    return <>
        <div className={onGoBack === undefined ? "flex flex-row justify-between float-end" : "flex flex-row justify-between"}>
            {onGoBack !== undefined && <button
                aria-label="Botão ir para voltar a tela anterior"
                className="text-white rounded-2xl bg-gray-500 hover:bg-gray-400 uppercase py-4 px-4 w-full mr-2"
                onClick={onGoBack}
            >
                {goBackText}
            </button>}
            <button
                aria-label="Botão ir para próxima tela"
                className=" text-white rounded-2xl bg-purple-700 hover:bg-purple-600 uppercase py-4 px-4 w-full ml-2"
                onClick={onNext}
            >
                {goNextText}
            </button>
        </div>
    </>;
});


export { ButtonNavigation }