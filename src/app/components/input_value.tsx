
import { amoutMask, currencyMask, kilometerMask, percentMask, timeMask } from '@/lib/masks';
import { memo, useEffect, useState } from 'react';
import MaskedInput from 'react-text-mask';


enum Formatation {
    Currency,
    Percentage,
    Time,
    Points,
    Kilometer,
    Amount
}

interface InputValueProps {
    value: string;
    formatation: Formatation;
    onChange: (value: string) => void;
}

const InputValue: React.FC<InputValueProps> = memo(({
    value,
    formatation,
    onChange
}) => {

    const [mask, setMask] = useState<any>();

    const formateValeu = () => {

        switch (formatation) {
            case Formatation.Currency:
                setMask(<MaskedInput
                    placeholder='R$ 0,00'
                    mask={currencyMask}
                    className="w-full rounded-lg p-5 text-center text-2xl block border border-gray-200 shadow-sm focus:outline-none focus:border-purple-500 placeholder-gray-200"
                    value={"R$ " + value}
                    onChange={(e) => onChange(e.target.value.replaceAll("R$ ", ""))}
                />)
                break;
            case Formatation.Percentage:
                setMask(<MaskedInput
                    placeholder='0,00 %'
                    mask={percentMask}
                    className="w-full rounded-lg p-5 text-center text-2xl block border border-gray-200 shadow-sm focus:outline-none focus:border-purple-500 placeholder-gray-200"
                    value={value + " %"}
                    onChange={(e) => onChange(e.target.value.replaceAll(" %", ""))}
                />)
                break;
            case Formatation.Time:
                setMask(<MaskedInput
                    placeholder='0,00 h'
                    mask={timeMask}
                    className="w-full rounded-lg p-5 text-center text-2xl block border border-gray-200 shadow-sm focus:outline-none focus:border-purple-500 placeholder-gray-200"
                    value={value + " h"}
                    onChange={(e) => onChange(e.target.value.replaceAll(" h", ""))}
                />)
                break;
            case Formatation.Points:
                break;
            case Formatation.Kilometer:
                setMask(<MaskedInput
                    placeholder='0,00 Km'
                    mask={kilometerMask}
                    className="w-full rounded-lg p-5 text-center text-2xl block border border-gray-200 shadow-sm focus:outline-none focus:border-purple-500 placeholder-gray-200"
                    value={value + " Km"}
                    onChange={(e) => onChange(e.target.value.replaceAll(" Km", ""))}
                />)
                break;
            case Formatation.Amount:
                setMask(<MaskedInput
                    placeholder='0 qtde'
                    mask={amoutMask}
                    className="w-full rounded-lg p-5 text-center text-2xl block border border-gray-200 shadow-sm focus:outline-none focus:border-purple-500 placeholder-gray-200"
                    value={value + " qtde"}
                    onChange={(e) => onChange(e.target.value.replaceAll(" qtde", ""))}
                />)
                break;
        }
    }

    useEffect(() => formateValeu(), [value])

    return <>
        <div className="mb-4 text-purple-700">
            <div className='flex flex-row justify-between pb-10'>
                {mask}
            </div>
        </div>
    </>
});


export { InputValue, Formatation };
