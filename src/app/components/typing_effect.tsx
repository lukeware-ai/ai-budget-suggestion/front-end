import { useState, useEffect } from "react";

interface TypingEffectProps {
    text: string;
    speed?: number;
}

const TypingEffect: React.FC<TypingEffectProps> = ({ text, speed = 10 }) => {
    const [displayedText, setDisplayedText] = useState<string>("");

    useEffect(() => {
        let index = 0;
        let currentText = "";

        const interval = setInterval(() => {
            if (index < text.length) {
                currentText += text.charAt(index);
                setDisplayedText(currentText);
                index++;
            } else {
                clearInterval(interval);
            }
        }, speed);
        return () => clearInterval(interval);
    }, [text, speed]);

    return <>{displayedText}</>;
};

export { TypingEffect };
