
const formateKilometer = (valor: number): string => {
  const valorFormatado = valor.toFixed(2).replace('.', ',');
  return `${valorFormatado} Km(s)`;
}


export { formateKilometer }