
const formatCurrency = (
  value: number,
  locale = "pt-BR",
  currency = "BRL"
) => {
  return new Intl.NumberFormat(locale, {
    style: "currency",
    currency: currency,
  }).format(value);
};

const parseCurrency = (formattedValue: string): number => {
  let value = formattedValue.replaceAll("R$ ", "");
  value = value.replaceAll(",", ".")
  const valueFinal = parseFloat(value);
  console.log(valueFinal)
  return isNaN(valueFinal) ? 0.0 : valueFinal;
};

export { formatCurrency, parseCurrency }