
const formatePoints = (valor: number): string => {
  const valorFormatado = valor.toFixed(2).replace('.', ',');
  return `${valorFormatado} Ponto(s)`;
}

const convertPoints = (valor: number, limitStart: number = 0, limitEnd: number = 10, max: number = 0.3): number => {
  if (valor < limitStart || valor > limitEnd) {
    throw new Error(`O valor deve estar entre ${limitStart} e ${limitEnd}.`);
  }
  return valor / limitEnd * max;
}

const reverseConvertPoints = (valorFracionado: number, limitStart: number = 0, limitEnd: number = 10, max: number = 0.3): number => {
  if (valorFracionado < 0 || valorFracionado > max) {
    throw new Error(`O valor fracionado deve estar entre ${limitStart} e ${max}.`);
  }
  return valorFracionado * (limitEnd / max);
}

export { formatePoints, convertPoints, reverseConvertPoints }