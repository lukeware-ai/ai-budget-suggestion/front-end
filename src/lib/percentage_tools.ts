
const formatPercentage = (value: number, locale = "pt-BR") => {
  return new Intl.NumberFormat(locale, {
    style: "percent",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  }).format(value / 100);
};


export { formatPercentage }