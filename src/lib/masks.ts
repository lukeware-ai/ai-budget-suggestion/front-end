// numberMask.ts
import createNumberMask from 'text-mask-addons/dist/createNumberMask';


const currencyMask = createNumberMask({
    prefix: 'R$ ',
    suffix: '',
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ',',
    integerLimit: 5, // Limite para 7 dígitos inteiros
    decimalLimit: 2, // Limite para 3 dígitos decimais
    allowNegative: false,
    allowLeadingZeroes: false,
});

const percentMask = createNumberMask({
    prefix: '',
    suffix: ' %',
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ',',
    integerLimit: 3, // Limite para 7 dígitos inteiros
    decimalLimit: 2, // Limite para 3 dígitos decimais
    allowNegative: false,
    allowLeadingZeroes: false,
});

const timeMask = createNumberMask({
    prefix: '',
    suffix: ' h',
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ',',
    integerLimit: 3, // Limite para 7 dígitos inteiros
    decimalLimit: 1, // Limite para 3 dígitos decimais
    allowNegative: false,
    allowLeadingZeroes: false,
});


const amoutMask = createNumberMask({
    prefix: '',
    suffix: ' qtde',
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ',',
    integerLimit: 2, // Limite para 7 dígitos inteiros
    decimalLimit: 0, // Limite para 3 dígitos decimais
    allowNegative: false,
    allowLeadingZeroes: false,
});
const kilometerMask = createNumberMask({
    prefix: '',
    suffix: ' Km',
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ',',
    integerLimit: 8, // Limite para 7 dígitos inteiros
    decimalLimit: 2, // Limite para 3 dígitos decimais
    allowNegative: false,
    allowLeadingZeroes: false,
});


export { currencyMask, percentMask, timeMask, amoutMask, kilometerMask }