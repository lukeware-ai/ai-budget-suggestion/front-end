
const formatTime = (minutes: number): string => {
  const hours = Math.floor(minutes / 60);
  const remainingMinutes = minutes % 60;
  return `${hours}h${remainingMinutes.toString().padStart(2, '0')}m`;
};

const convertToFraction = (timeString: string): number => {
  const [hours, minutes] = timeString.split("h").map((value) => parseInt(value));
  const fraction = hours + (minutes / 60);
  return fraction;
};

const convertToNumber = (time: number): number => {
  return time * 60;
};

export { formatTime, convertToFraction, convertToNumber }