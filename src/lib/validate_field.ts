type BudgetParameters = {
  hourlyRate: number;
  workTime: number;
  materialRate: number;
  complexity: number;
  costLunch: number;
  distanceWork: number;
  costFuel: number;
  vehicleMaintenanceRate: number;
  tax: number;
  workToolsRate: number;
  employeeWorkTime: number;
  costEmployeeHourly: number;
  numberEmployees: number;
  costMaterial: number;
};

const defaultValues: Partial<BudgetParameters> = {
  hourlyRate: 0,
  workTime: 0,
  materialRate: 0,
  complexity: 0,
  costLunch: 0,
  distanceWork: 0,
  costFuel: 0,
  vehicleMaintenanceRate: 0,
  tax: 0,
  workToolsRate: 0,
  employeeWorkTime: 0,
  costEmployeeHourly: 0,
  numberEmployees: 0,
  costMaterial: 0
};

const fillUndefinedFields = <T extends Record<string, any>>(
  obj: T,
  defaultValues: Partial<T> = {}
): T => {
  const filledObj = { ...obj };
  for (const key in filledObj) {
    if (filledObj[key] === undefined) {
      filledObj[key] = defaultValues[key as keyof T] as T[Extract<keyof T, string>];
    }
  }
  return filledObj;
};

export { fillUndefinedFields, defaultValues };
