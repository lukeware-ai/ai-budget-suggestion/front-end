import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { RadioButtons } from '@/app/components/radio_buttons';

import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import { convertPoints, reverseConvertPoints } from '@/lib/points_tools';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const Complexity: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [complexity, setComplexity] = useState<string>("");

  const onHandlerBack = () => router.push("/work_time");
  const onHandlerNext = () => {
    updateBudgetParameter('complexity', Number(complexity) / 10)
    router.push("/number_employees")
  }

  useEffect(() => {
    if (budgetParameters) {
      setComplexity(String(budgetParameters.complexity * 10));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Complexidade do serviço</title>
        <meta name="description" content="Defina o nível de complexidade do serviço. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo o nível de complexidade do serviço'
        description='Que tal você me dizer o quão complicado é o serviço que você quer fazer?'
        onGoHome={() => router.push("/")}
      >
        <RadioButtons
          selectedValue={complexity}
          onChange={(e) => setComplexity(e)}
          options={[
            { label: "Nenhuma", value: "0" },
            { label: "Baixa", value: "1" },
            { label: "Moderada", value: "2" },
            { label: "Alta", value: "3" }
          ]} />



        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default Complexity;
