
import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const HourlyRate: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();

  const [hourlyRate, setHourlyRate] = useState<string>("0");


  const onHandlerBack = () => router.push("/");
  const onHandlerNext = () => {
    const value: number = parseFloat(hourlyRate.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('hourlyRate', Number(value.toFixed(2)));
      router.push("/work_time");
    }
  };

  useEffect(() => {
    if (budgetParameters) {
      setHourlyRate(String(budgetParameters.hourlyRate).replaceAll(".", ","));
    }
  }, [budgetParameters]);


  return (
    <>
      <Head>
        <title>Preço por Hora</title>
        <meta name="description" content="Defina sua tarifa horária para o serviço que você oferece. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        description='Quanto você está pensando em cobrar por hora para esse serviço?'
        title='Definindo seu Preço por Hora'
        onGoHome={() => router.push("/")}
      >
        <InputValue
          value={hourlyRate}
          formatation={Formatation.Currency}
          onChange={(value: string) => setHourlyRate(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default HourlyRate;
