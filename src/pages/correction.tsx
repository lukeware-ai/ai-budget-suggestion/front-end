
import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const Correction: React.FC = () => {
  const { budgetParameters, clearBudgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [valueState, setValueState] = useState<string>("0");

  const onHandlerBack = () => router.push("/");
  const onFinishing = () => {
    const value: number = parseFloat(valueState.replaceAll(",", "."))
    if (value !== undefined) {
      const newValue = Number(value.toFixed(2))
      updateBudgetParameter('label', newValue);
      fetchData(newValue)
    }

  }

  const fetchData = async (newValue: number) => {
    try {
      const payload = budgetParameters
      payload.label = newValue

      console.log(newValue)

      const response = await fetch("/api/instance", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error("Falha ao buscar previsão");
      }
      router.push("/thank_you");
      clearBudgetParameters();
    } catch (error: any) {
      console.error(error);
    }
  };

  useEffect(() => {
    setValueState(String(budgetParameters.label).replaceAll(".", ","))
  }, [budgetParameters]);


  return (
    <>
      <Head>
        <title>Refinar o valor do orçamento</title>
        <meta name="description" content="Ajustando o valor sugerido pela IA" />
      </Head>

      <Template
        title='Ajustando com o novo valor sugerido'
        description='Desculpe pelo erro, o valor está bem fora da realidade. Qual seria o valor que você gostaria de ajustar?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={valueState}
          formatation={Formatation.Currency}
          onChange={(value: string) => setValueState(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onFinishing} goNextText='Finalizar' />
      </Template>
    </>
  );
};

export default Correction;
