import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const DistanceWork: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [distanceWork, setDistanceWork] = useState<string>("0");

  const onHandlerBack = () => router.push("/cost_fuel");
  const onHandlerNext = () => {
    const value: number = parseFloat(distanceWork.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('distanceWork', Number(value.toFixed(2)));
      router.push("/vehicle_maintenance_rate");
    }
  };

  useEffect(() => {
    if (budgetParameters) {
      setDistanceWork(String(budgetParameters.distanceWork).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Distância do serviço</title>
        <meta name="description" content="Defina a distância do serviço. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo a distância do serviço'
        description='Fala aí, qual é a distância até o serviço?'
        onGoHome={() => router.push("/")}
      >
        <InputValue
          value={distanceWork}
          formatation={Formatation.Kilometer}
          onChange={(value: string) => setDistanceWork(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default DistanceWork;
