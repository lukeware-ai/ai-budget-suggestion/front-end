
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import { formatCurrency } from '@/lib/currency_tools';
import { defaultValues, fillUndefinedFields } from '@/lib/validate_field';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { FaThumbsDown, FaThumbsUp } from "react-icons/fa6";

const Completion: React.FC = () => {
  const { budgetParameters, clearBudgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [value, setValue] = useState<number>(0);
  const onFinishing = () => {
    updateBudgetParameter('label', value)
    updateAI()
  }

  const updateAI = async () => {
    try {
      const payload = budgetParameters
      payload.label = value

      const response = await fetch("/api/instance", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error("Falha ao buscar previsão");
      }
      router.push("/");
      clearBudgetParameters()
    } catch (error: any) {
      console.error(error);
    }
  };

  const fetchData = async () => {
    try {
      const payload = fillUndefinedFields(budgetParameters, defaultValues)

      const response = await fetch("/api/predict", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error("Falha ao buscar previsão");
      }

      const data = await response.json();
      setValue(Number(data.data.label))
      updateBudgetParameter('label', Number(data.data.label))
    } catch (error: any) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);


  return (
    <>
      <Head>
        <title>Resumo</title>
        <meta name="description" content="Parabéns, chegamos ao resumo com o valor sugerido para o seu orçamento" />
      </Head>

      <Template
        title='Parabéns, chegamos ao fim, ufa!'
        description='Fiz os cálculos com todas as informações que você me passou e gerei um valor para o seu orçamento. O que você acha?'
        onGoHome={onFinishing}
      >

        <p className='text-purple-700 font-medium text-3xl text-center m-10'>{formatCurrency(value)}</p>


        <div className='flex flex-row justify-around pb-10'>
          <button
            aria-label="Diminuir valor"
            className="flex items-center justify-center text-white p-6 rounded-2xl bg-gray-500 hover:bg-gray-400 mb-2 md:mb-0 md:mr-0"
            onClick={() => { router.push("/correction") }}
          >
            <FaThumbsDown size={24} />
          </button>
          <button
            aria-label="Aumentar valor"
            className="flex items-center justify-center text-white p-6 rounded-2xl bg-purple-700 hover:bg-purple-600 mb-2 md:mb-0 md:mr-0"
            onClick={onFinishing}
          >
            <FaThumbsUp size={24} />
          </button>
        </div>


      </Template>
    </>
  );
};

export default Completion;
