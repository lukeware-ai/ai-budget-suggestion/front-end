import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const CostEmployeeHourly: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();

  const [costEmployeeHourly, setCostEmployeeHourly] = useState<string>("0");

  const onHandlerBack = () => router.push("/employee_work_time");
  const onHandlerNext = () => {
    const value: number = parseFloat(costEmployeeHourly.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('costEmployeeHourly', Number(value.toFixed(2)));
      router.push("/cost_lunch");
    }
  }

  useEffect(() => {
    if (budgetParameters) {
      setCostEmployeeHourly(String(budgetParameters.costEmployeeHourly).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Custo da Hora do Funcionário</title>
        <meta name="description" content="Defina quanto custa a hora do seu funcionário. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo o preço da hora do funcionário'
        description='Quanto está custando a hora do seu funcionário?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={costEmployeeHourly}
          formatation={Formatation.Currency}
          onChange={(value: string) => setCostEmployeeHourly(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default CostEmployeeHourly;
