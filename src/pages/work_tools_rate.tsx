import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const WorkToolsRate: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [workToolsRate, setWorkToolsRate] = useState<string>("0");

  const onHandlerBack = () => router.push("/material_rate");
  const onHandlerNext = () => {
    const value: number = parseFloat(workToolsRate.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('workToolsRate', Number(value.toFixed(2)) / 100);
      router.push("/tax");
    }
  }

  useEffect(() => {
    if (budgetParameters) {
      setWorkToolsRate(String(budgetParameters.workToolsRate * 100).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Custo Manutenção Ferramentas</title>
        <meta name="description" content="Defina quantos por cento deseja cobrar para manutenção das suas ferramentas. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo margem de manutenção das ferramentas'
        description='Quantos % você quer adicionar no preço final para manutenção das ferramentas?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={workToolsRate}
          formatation={Formatation.Percentage}
          onChange={(value: string) => setWorkToolsRate(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default WorkToolsRate;
