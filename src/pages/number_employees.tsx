import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const NumberEmployees: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [numberEmployees, setNumberEmployees] = useState<string>("0");

  const onHandlerBack = () => router.push("/complexity");
  const onHandlerNext = () => {
    const value: number = parseFloat(numberEmployees.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('numberEmployees', Number(value.toFixed(2)))
      router.push("/employee_work_time")
    }
  }

  useEffect(() => {
    if (budgetParameters) {
      setNumberEmployees(String(budgetParameters.numberEmployees).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Quantidade de Funcionários</title>
        <meta name="description" content="Defina a quantidade de funcionários que vão atuar no serviço. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo a quantidade de funcionários para o serviço'
        description='Quantos funcionários vão trabalhar neste serviço?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={numberEmployees}
          formatation={Formatation.Amount}
          onChange={(value: string) => setNumberEmployees(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default NumberEmployees;
