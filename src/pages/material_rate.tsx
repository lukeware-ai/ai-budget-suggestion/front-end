import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const MaterialRate: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [materialRate, setMaterialRate] = useState<string>("0");

  const onHandlerBack = () => router.push("/cost_material");
  const onHandlerNext = () => {
    const value: number = parseFloat(materialRate.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('materialRate', Number(value.toFixed(2)) / 100);
      router.push("/work_tools_rate");
    }
  }

  useEffect(() => {
    if (budgetParameters) {
      setMaterialRate(String(budgetParameters.materialRate * 100).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Custo Material</title>
        <meta name="description" content="Defina sua margem de custo dos materiais para o serviço que você oferece. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo margem de lucro sobre os materiais'
        description='Qual porcentagem de lucro você deseja aplicar aos materiais utilizados no serviço?'
        onGoHome={() => router.push("/")}
      >
        <InputValue
          value={materialRate}
          formatation={Formatation.Percentage}
          onChange={(value: string) => setMaterialRate(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default MaterialRate;
