import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const VehicleMaintenanceRate: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [vehicleMaintenanceRate, setVehicleMaintenanceRate] = useState<string>("0");

  const onHandlerBack = () => router.push("/distance_work");
  const onHandlerNext = () => {
    const value: number = parseFloat(vehicleMaintenanceRate.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('vehicleMaintenanceRate', Number(value.toFixed(2)) / 100);
      router.push("/cost_material"); 
    }
  }

  useEffect(() => {
    if (budgetParameters) {
      setVehicleMaintenanceRate(String(budgetParameters.vehicleMaintenanceRate * 100).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Custo Manutenção Automóvel</title>
        <meta name="description" content="Defina quantos por cento deseja cobrar para manutenção do veículo com base no valor hora cobrado. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo margem de manutenção do automóvel'
        description='Quantos % você quer adicionar no preço final para manutenção do carro?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={vehicleMaintenanceRate}
          formatation={Formatation.Percentage}
          onChange={(value: string) => setVehicleMaintenanceRate(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default VehicleMaintenanceRate;
