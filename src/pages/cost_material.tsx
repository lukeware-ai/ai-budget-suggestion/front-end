import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const CostMaterial: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();

  const [costMaterial, setCostMaterial] = useState<string>("0");


  const onHandlerBack = () => router.push("/vehicle_maintenance_rate");
  const onHandlerNext = () => {
    const value: number = parseFloat(costMaterial.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('costMaterial', Number(value.toFixed(2)));
      router.push("/material_rate");
    }
  };

  useEffect(() => {
    if (budgetParameters) {
      setCostMaterial(String(budgetParameters.costMaterial).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Custo dos materiais</title>
        <meta name="description" content="Defina o custo total dos materiais. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo o custo dos materiais'
        description='Qual será o custo dos materiais necessários para realizar esta tarefa?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={costMaterial}
          formatation={Formatation.Currency}
          onChange={(value: string) => setCostMaterial(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default CostMaterial;
