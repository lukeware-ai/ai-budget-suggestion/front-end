import { useRouter } from "next/router";
import React from "react";

import "@/app/globals.css";

const Question1: React.FC = () => {
  const router = useRouter();

  return (
    <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
      <div className="bg-white p-6 rounded-lg shadow-lg w-full max-w-md">
        <h1 className="text-base sm:text-2xl font-bold mb-4 text-gray-700 uppercase">
          Muito obrigado!
        </h1>
        <div>
          <h2 className="text-[17px] sm:text-xl font-light mb-4 text-gray-700 text-justify">
            Desculpe pela confusão com o valor sugerido, mas sua ajuda é fundamental em
            meu aprendizado! Valeu mesmo!
          </h2>
          <p className="text-[12px] font-bold sm:text-xl mb-4 text-gray-700 text-justify">
            #MuitoTop
          </p>
        </div>

        <div className="flex flex-col md:flex-row justify-between md:float-right">
          <button
            className="bg-purple-600 text-white py-2 px-4 md:px-10 rounded hover:bg-purple-700"
            onClick={() => router.push("/")}
          >
            Ir para Início
          </button>
        </div>
      </div>
    </div>
  );
};

export default Question1;
