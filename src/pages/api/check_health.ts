import type { NextApiRequest, NextApiResponse } from 'next';

type Data = {
  status: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  try {
    const apiUrl = process.env.NEXT_PUBLIC_API_URL;
    const response = await fetch(`${apiUrl}/health`);
    const data = await response.json();

    if (response.ok) {
      res.status(200).json({ status: data.status });
    } else {
      res.status(response.status).json({ status: 'error' });
    }
  } catch (error) {
    res.status(500).json({ status: 'error' });
  }
}
