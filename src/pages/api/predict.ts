import { NextApiRequest, NextApiResponse } from "next";

type BudgetParameters = {
  hourlyRate: number;
  workTime: number;
  materialRate: number;
  complexity: number;
  costLunch: number;
  distanceWork: number;
  costFuel: number;
  vehicleMaintenanceRate: number;
  tax: number;
  workToolsRate: number;
  employeeWorkTime: number;
  costEmployeeHourly: number;
  numberEmployees: number;
  costMaterial: number;
};

type PredictResponseData = {
  label: number;
};

type PredictResponse = {
  data: PredictResponseData;
  status: string;
};

type PredictErrorResponse = {
  status: string;
  message: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<PredictResponse | PredictErrorResponse>
) {
  if (req.method !== "POST") {
    return res
      .status(405)
      .json({ status: "error", message: "Method Not Allowed" });
  }

  try {
    const {
      hourlyRate,
      workTime,
      materialRate,
      complexity,
      costLunch,
      distanceWork,
      costFuel,
      vehicleMaintenanceRate,
      tax,
      workToolsRate,
      employeeWorkTime,
      costEmployeeHourly,
      numberEmployees,
      costMaterial
    } = req.body as BudgetParameters;

    let data_request = {
      e_valor_hora: hourlyRate,
      e_tmp_servico: workTime,
      e_margem_material: materialRate,
      e_complexidade_1_3: complexity,
      s_valor_almoco: costLunch,
      s_distancia_do_servico_km: distanceWork,
      s_preco_combustivel: costFuel,
      s_custo_manut_autom: vehicleMaintenanceRate,
      s_custo_imposto: tax,
      s_custo_ferram: workToolsRate,
      s_tmp_servico_func: employeeWorkTime,
      s_valor_hora_func: costEmployeeHourly,
      s_qt_func: numberEmployees,
      s_valor_materia: costMaterial
    }

    const data_request_convert = JSON.stringify(data_request);


    const apiUrl = process.env.NEXT_PUBLIC_API_URL;
    const response = await fetch(
      `${apiUrl}/predict`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: data_request_convert,
      }
    );

    if (!response.ok) {
      throw new Error(`Failed to fetch data from prediction service: ${response}`);
    }

    const data = await response.json();
    res.status(200).json(data);
  } catch (error: any) {
    res.status(500).json({ status: "error", message: error.message });
  }
}
