
import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const CostLunch: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();

  const [costLunch, setCostLunch] = useState<string>("0");

  const onHandlerBack = () => router.push("/cost_employee_hourly");
  const onHandlerNext = () => {
    const value: number = parseFloat(costLunch.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('costLunch', Number(value.toFixed(2)));
      router.push("/cost_fuel");
    }
  };

  useEffect(() => {
    if (budgetParameters) {
      setCostLunch(String(budgetParameters.costLunch).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Custo do almoço</title>
        <meta name="description" content="Defina o custo total do almoço. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo o custo do almoço'
        description='Quanto você vai precisar gastar com almoço?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={costLunch}
          formatation={Formatation.Currency}
          onChange={(value: string) => setCostLunch(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default CostLunch;
