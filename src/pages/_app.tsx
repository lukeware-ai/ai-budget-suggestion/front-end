import React from 'react';
import { AppProps } from 'next/app';
import { BudgetParametersProvider} from '@/contexts/budget_parameters_context';
import "@/app/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <BudgetParametersProvider>
      <Component {...pageProps} />
    </BudgetParametersProvider>
  );
}

export default MyApp;
