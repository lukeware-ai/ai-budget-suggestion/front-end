
import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const CostFuel: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();

  const [costFuel, setCostFuel] = useState<string>("0");

  const onHandlerBack = () => router.push("/cost_lunch");
  const onHandlerNext = () => {
    const value: number = parseFloat(costFuel.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('costFuel', Number(value.toFixed(2)));
      router.push("/distance_work");
    }
  };

  useEffect(() => {
    if (budgetParameters) {
      setCostFuel(String(budgetParameters.costFuel).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Preço do combustível</title>
        <meta name="description" content="Defina o preço do combustível. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo o preço do combustível'
        description='Quanto está o preço do combustível hoje?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={costFuel}
          formatation={Formatation.Currency}
          onChange={(value: string) => setCostFuel(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default CostFuel;
