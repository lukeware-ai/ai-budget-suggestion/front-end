import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const WorkTime: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [workTime, setWorkTime] = useState<string>("0");


  const onHandlerBack = () => router.push("/hourly_rate")
  const onHandlerNext = () => {
    const value: number = parseFloat(workTime.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('workTime', Number(value.toFixed(2)))
      router.push("/complexity")
    }
  }

  useEffect(() => {
    if (budgetParameters) {
      setWorkTime(String(budgetParameters.workTime).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Tempo Estimado</title>
        <meta name="description" content="Aqui, você define o tempo estimado para concluir seu serviço, ajustando o controle deslizante. Escolha com precisão deslizando o controle para a quantidade de tempo necessária." />
      </Head>

      <Template
        title='Defina o tempo que vai levar'
        description='Você tem ideia de quanto tempo vai levar para concluir esse serviço?'
        onGoHome={() => router.push("/")}
      >
        <InputValue
          value={workTime}
          formatation={Formatation.Time}
          onChange={(value: string) => setWorkTime(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default WorkTime;
