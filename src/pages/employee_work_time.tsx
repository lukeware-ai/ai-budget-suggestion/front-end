import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const EmployeeWorkTime: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [employeeWorkTime, setEmployeeWorkTime] = useState<string>("0");


  const onHandlerBack = () => router.push("/number_employees");
  const onHandlerNext = () => {
    const value: number = parseFloat(employeeWorkTime.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('employeeWorkTime', Number(value.toFixed(2)));
      router.push("/cost_employee_hourly");
    }
  };

  useEffect(() => {
    if (budgetParameters) {
      setEmployeeWorkTime(String(budgetParameters.employeeWorkTime).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Tempo Estimado do Funcionário</title>
        <meta name="description" content="Aqui, você define o tempo estimado do funcionário que leva para concluir seu serviço, ajustando o controle deslizante. Escolha com precisão deslizando o controle para a quantidade de tempo necessária." />
      </Head>

      <Template
        title='Definindo o tempo do funcionário'
        description='Você tem ideia de quanto tempo o funcionário/ajudante vai levar para concluir esse serviço?'
        onGoHome={() => router.push("/")}
      >
        <InputValue
          value={employeeWorkTime}
          formatation={Formatation.Time}
          onChange={(value: string) => setEmployeeWorkTime(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default EmployeeWorkTime;
