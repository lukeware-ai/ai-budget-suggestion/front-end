import { ButtonNavigation } from '@/app/components/button_navigation';
import { Formatation, InputValue } from '@/app/components/input_value';
import { Template } from '@/app/components/template';
import { useBudgetParameters } from '@/contexts/budget_parameters_context';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const Tax: React.FC = () => {
  const { budgetParameters, updateBudgetParameter } = useBudgetParameters();
  const router = useRouter();
  const [tax, setTax] = useState<string>("0");

  const onHandlerBack = () => router.push("/work_tools_rate");
  const onHandlerNext = () => {
    const value: number = parseFloat(tax.replaceAll(",", "."))
    if (value !== undefined) {
      updateBudgetParameter('tax', Number(value.toFixed(2)) / 100);
      router.push("/completion");
    }
  }

  useEffect(() => {
    if (budgetParameters) {
      setTax(String(budgetParameters.tax * 100).replaceAll(".", ","));
    }
  }, [budgetParameters]);

  return (
    <>
      <Head>
        <title>Custo do Imposto</title>
        <meta name="description" content="Defina quantos por cento de imposto você quer separar para o governo. Escolha o valor desejado deslizando o controle." />
      </Head>

      <Template
        title='Definindo custo do imposto'
        description='Quantos % de imposto sobre o valor você quer separar?'
        onGoHome={() => router.push("/")}
      >

        <InputValue
          value={tax}
          formatation={Formatation.Percentage}
          onChange={(value: string) => setTax(value)}
        />

        <ButtonNavigation onGoBack={onHandlerBack} onNext={onHandlerNext} />
      </Template>
    </>
  );
};

export default Tax;
